package com.buzzhiraassessment.Exercise1;

import java.util.ArrayList;
import java.util.Collections;

/**
 * João was here on 22/12/2016.
 */

public class MathUtils {


    /**
     * Returns the length of n's longest plateau.
     */

    public static int solution(int n) {

        String binaryString = Integer.toBinaryString(n);
        int sequenceCount = 0;
        ArrayList<Integer> countsArray = new ArrayList<>();

        for (int charIndex = 0; charIndex < binaryString.length(); charIndex++) {

            char character = binaryString.charAt(charIndex);

            if (character == '1') {
                sequenceCount = sequenceCount + 1;
            }
            else {
                countsArray.add(sequenceCount);
                sequenceCount = 0;
            }
        }

        return (Collections.max(countsArray) == 1) ? 0 : Collections.max(countsArray);
    }

}
