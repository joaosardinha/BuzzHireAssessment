package com.buzzhiraassessment.CalendarDecorators;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.style.LineBackgroundSpan;
import android.util.TypedValue;

/**
 * João was here on 22/12/2016.
 */

public class CustomDotSpan implements LineBackgroundSpan {

    /**
     * Default radius used
     */
    public static final float DEFAULT_RADIUS = 3;

    private final float radius;
    private final int color;
    public float topPadding;

    /**
     * Create a span to draw a dot using default radius and color
     *
     * @see #LeftDotSpan(float, int)
     * @see #DEFAULT_RADIUS
     */
    public CustomDotSpan() {
        this.radius = DEFAULT_RADIUS;
        this.color = 0;
    }

    /**
     * Create a span to draw a dot using a specified color
     *
     * @param color color of the dot
     * @see #LeftDotSpan(float, int)
     * @see #DEFAULT_RADIUS
     */
    public CustomDotSpan(int color) {
        this.radius = DEFAULT_RADIUS;
        this.color = color;
    }

    /**
     * Create a span to draw a dot using a specified radius
     *
     * @param radius radius for the dot
     * @see #DotSpan(float, int)
     */
    public CustomDotSpan(float radius) {
        this.radius = radius;
        this.color = 0;
    }

    /**
     * Create a span to draw a dot using a specified radius and color
     *
     * @param radius radius for the dot
     * @param color  color of the dot
     */
    public CustomDotSpan(int color, float topPadding, Context mContext) {
        this.radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, mContext.getResources().getDisplayMetrics());
        this.color = color;
        this.topPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, topPadding, mContext.getResources().getDisplayMetrics());
    }

    @Override
    public void drawBackground(
            Canvas canvas, Paint paint,
            int left, int right, int top, int baseline, int bottom,
            CharSequence charSequence,
            int start, int end, int lineNum) {

        int oldColor = paint.getColor();
        if (color != 0) {
            paint.setColor(color);
        }
        canvas.drawCircle(((left + right) / 2) , bottom + radius + topPadding, radius, paint);


        paint.setColor(oldColor);
    }
}

