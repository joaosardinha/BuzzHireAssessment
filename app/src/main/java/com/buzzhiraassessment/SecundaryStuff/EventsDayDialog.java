package com.buzzhiraassessment.SecundaryStuff;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buzzhiraassessment.R;

/**
 * João was here on 22/12/2016.
 */

public class EventsDayDialog extends DialogFragment {

    TextView title;
    LinearLayout eventsContainer;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_events_day, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        title = (TextView)v.findViewById(R.id.eventsDialogTitle);
        eventsContainer = (LinearLayout)v.findViewById(R.id.eventsListContainer);

        title.append(getArguments().getString("date"));

        for (int i = 1; i <= getArguments().getInt("eventsAmount", 0); i++) {
            EventsView eventsView = new EventsView(getActivity(),
                    getArguments().getInt("color"+i),
                    getArguments().getString("name"+i),
                    getArguments().getString("time"+i)
            );
            eventsContainer.addView(eventsView);
        }



        v.findViewById(R.id.eventsdialogroot).setMinimumWidth((int) ( Resources.getSystem().getDisplayMetrics().widthPixels * .9f));
        v.findViewById(R.id.closedialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        return v;
    }
}