package com.buzzhiraassessment.SecundaryStuff;

import com.prolificinteractive.materialcalendarview.CalendarDay;

/**
 * João was here on 22/12/2016.
 */

public class MonthEvent {

    public CalendarDay date;
    public String time;
    public int color;
    public String name;

    public MonthEvent(CalendarDay date, String time, int color, String name) {
        this.date = date;
        this.time = time;
        this.color = color;
        this.name = name;
    }

}
